public class Facultad {
    private String nombre;
    private Universidad universidad;
    
    public Facultad(String nombre, Universidad universidad){
        this.nombre=nombre;
        this.universidad=universidad;
    }
    /**
     * Metodo consultor (getters)
     */
    public String getNombre(){
        return this.nombre;
    }
    /**
     * Metodo modificador (Setters)
     */
    public void setNombre(String nombre){
        this.nombre=nombre;
    }
    public void crear_carreras(String nombre){
        System.out.println("Crear carrera "+nombre);
    }
    
}
