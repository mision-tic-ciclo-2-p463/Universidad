/**
 * Autor: Juan Romero
 * Empresa: UTP - Mintic
 * Descripción: Ejemplo de composicion
 * Año: 2021
 * Gitlab:
 */
public class Universidad {
  /**
   * Atributos
   * */
  private String nombre;
  private String nit;
  private Facultad [] facultades;
  
  /*
  * Constructor
  */
  public Universidad(String nombre, String nit){
      this.nombre=nombre;
      this.nit=nit;
      this.facultades=new Facultad[5];

  }
  /*public Universidad(){
      this.nombre="";
      this.nit="";
  }*/
  public Facultad getFacultades(int pos){
      return this.facultades[pos];

  }

  //metodos
  public void crear_facultad(String nombre, int pos){
      Facultad objFacultad=new Facultad(nombre,this);
      this.facultades[pos] = objFacultad;
  }
  ///comentarios
}
